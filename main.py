import os

from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

#binary = FirefoxBinary(r"D:\torbrowser\Tor Browser\Browser\firefox.exe")
profile = FirefoxProfile(r"D:\torbrowser\Tor Browser\Browser\TorBrowser\Data\Browser\profile.default")
torexe = os.popen(r"D:\torbrowser\Tor Browser\Browser\TorBrowser\Tor\tor.exe")

profile.set_preference('network.proxy.type', 1)
profile.set_preference('network.proxy.socks', '127.0.0.1')
profile.set_preference('network.proxy.socks_port', 9050)
profile.set_preference("network.proxy.socks_remote_dns", False)
profile.update_preferences()
driver = webdriver.Firefox(profile, executable_path=r"D:\Descargas\Programs\geckodriver.exe")
driver.get("https://check.torproject.org/")

